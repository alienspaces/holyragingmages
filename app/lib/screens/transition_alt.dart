import 'dart:async' as dartAsync;
import 'dart:ui';
import 'package:flame/game.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:flame/components.dart';

// Application packages

class TransitionGame extends Game {
  // Mage image files
  List<String> imageFiles = [
    'mages/dark-armoured/attack/attack_000.png',
    'mages/dark-armoured/attack/attack_001.png',
    'mages/dark-armoured/attack/attack_003.png',
    'mages/dark-armoured/attack/attack_004.png',
    'mages/dark-armoured/attack/attack_005.png',
    'mages/dark-armoured/attack/attack_006.png',
    'mages/dark-armoured/attack/attack_007.png',
    'mages/dark-armoured/attack/attack_008.png',
    'mages/dark-armoured/attack/attack_009.png',
    'mages/dark-armoured/attack/attack_010.png',
    'mages/dark-armoured/attack/attack_011.png',
    'mages/dark-armoured/attack/attack_010.png',
    'mages/dark-armoured/attack/attack_009.png',
    'mages/dark-armoured/attack/attack_008.png',
    'mages/dark-armoured/attack/attack_007.png',
    'mages/dark-armoured/attack/attack_006.png',
  ];

  // Mage
  SpriteAnimationComponent mage;

  TransitionGame() {
    initialise();
  }

  void initialise() async {
    // Logger
    final log = Logger('TransitionGame - initialise');

    log.fine("Initialising");
  }

  @override
  Future<void> onLoad() async {
    await images.loadAll(imageFiles);

    List<Sprite> sprites = [];
    for (var i = 0; i < imageFiles.length; i++) {
      Sprite sprite = await Sprite.load(imageFiles[i]);
      sprites.add(sprite);
    }
    final size = Vector2.all(128.0);
    mage = SpriteAnimationComponent();
    mage.size = size;
    mage.animation = SpriteAnimation.spriteList(
      sprites,
      stepTime: 0.07,
      loop: false,
    );
  }

  void resize(Size size) {}

  void render(Canvas canvas) {
    // Logger
    final log = Logger('TransitionGame - render');

    log.fine("Rendering");
    mage.render(canvas);
  }

  void update(double t) {
    // Logger
    final log = Logger('TransitionGame - update');

    log.fine("Updating - $t");
    mage.update(t);
  }
}

class TransitionScreen extends StatefulWidget {
  final String routeName;
  final String animationName;

  TransitionScreen({Key key, this.routeName, this.animationName}) : super(key: key);

  @override
  _TransitionScreenState createState() => _TransitionScreenState();
}

class _TransitionScreenState extends State<TransitionScreen> {
  dartAsync.Timer timer;
  static const duration = const Duration(seconds: 3);

  @override
  void initState() {
    final log = Logger('TransitionScreen - initState');
    log.warning('Initialise transition screen');

    timer = dartAsync.Timer(duration, handleRouting);
    super.initState();
  }

  @override
  void dispose() {
    final log = Logger('TransitionScreen - dispose');
    log.warning('Disposing transition screen');
    if (mounted) {
      timer.cancel();
    }
    super.dispose();
  }

  void handleRouting() {
    final log = Logger('TransitionScreen - handleRouting');
    log.warning('Routing to ${widget.routeName}');

    // Navigator.of(context).pushReplacementNamed(widget.routeName);
  }

  @override
  Widget build(BuildContext context) {
    // Logger
    final log = Logger('TransitionScreen - build');

    log.info("Building");

    TransitionGame game = TransitionGame();

    return Scaffold(
      appBar: AppBar(
        title: Text('Transition'),
      ),
      body: Container(
        child: GameWidget(
          game: game,
        ),
      ),
      resizeToAvoidBottomInset: false,
    );
  }
}
