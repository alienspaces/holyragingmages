import 'dart:async' as dartAsync;
import 'package:flame/game.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';

// Application packages
import 'package:holyragingmages/components/transition_mage.dart';

class TransitionGame extends BaseGame {
  @override
  Future<void> onLoad() async {
    add(TransitionMageComponent());
  }
}

class TransitionScreen extends StatefulWidget {
  final String routeName;
  final String animationName;

  TransitionScreen({Key key, this.routeName, this.animationName}) : super(key: key);

  @override
  _TransitionScreenState createState() => _TransitionScreenState();
}

class _TransitionScreenState extends State<TransitionScreen> {
  dartAsync.Timer timer;
  static const duration = const Duration(seconds: 3);

  @override
  void initState() {
    final log = Logger('TransitionScreen - initState');
    log.warning('Initialise transition screen');

    timer = dartAsync.Timer(duration, handleRouting);
    super.initState();
  }

  @override
  void dispose() {
    final log = Logger('TransitionScreen - dispose');
    log.warning('Disposing transition screen');
    if (mounted) {
      timer.cancel();
    }
    super.dispose();
  }

  void handleRouting() {
    final log = Logger('TransitionScreen - handleRouting');
    log.warning('Routing to ${widget.routeName}');

    // Navigator.of(context).pushReplacementNamed(widget.routeName);
  }

  @override
  Widget build(BuildContext context) {
    // Logger
    final log = Logger('TransitionScreen - build');

    log.info("Building");

    TransitionGame game = TransitionGame();

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Transition'),
      ),
      body: Container(
        child: GameWidget(
          game: game,
        ),
      ),
      resizeToAvoidBottomInset: false,
    );
  }
}
