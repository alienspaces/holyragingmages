import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:logging/logging.dart';

// Application packages
import 'package:holyragingmages/api/api.dart';
import 'package:holyragingmages/models/models.dart';

class LoadingScreen extends StatefulWidget {
  final Api api;

  LoadingScreen({Key key, this.api}) : super(key: key);

  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // Logger
    final log = Logger('LoadingScreen - build');

    log.info("Building");

    return ChangeNotifierProvider<Mage>(
      create: (context) => Mage(api: widget.api),
      child: Scaffold(
        appBar: AppBar(
          title: Text('Loading'),
        ),
        body: Container(),
        resizeToAvoidBottomInset: false,
      ),
    );
  }
}
