import 'package:flutter/material.dart';
import 'package:logging/logging.dart';

class ChooseMageButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Logger
    final log = Logger('ChooseMageButtonWidget - build');

    log.info("Building");

    return Container(
      child: TextButton(
        onPressed: null,
        child: Text('Next'),
      ),
    );
  }
}
