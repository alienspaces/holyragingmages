// import 'dart:math';

import 'package:flame/components.dart';
import 'package:flutter/material.dart';
import 'package:flame/game.dart';
import 'package:flame/sprite.dart';

// Application packages
import 'package:holyragingmages/screens/transition.dart';

class TransitionMageComponent extends SpriteBatchComponent with HasGameRef<TransitionGame> {
  @override
  Future<void> onLoad() async {
    SpriteBatch spriteBatch =
        await gameRef.loadSpriteBatch('mages/red-stripe-necromancer/attack/spritesheet.png');

    // app/assets/images/mages/red-stripe-necromancer/attack/spritesheet.png
    double mageCount = 10;
    for (double i = 0; i < mageCount; ++i) {
      spriteBatch.add(
        source: Rect.fromLTWH(i * 500, 0, 500, 420),
        offset: Vector2.all(100),
      );
    }
  }
}
