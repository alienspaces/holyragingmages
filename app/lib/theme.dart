import 'package:flutter/material.dart';

ThemeData getTheme() {
  return ThemeData(
    // Define the default brightness and colors.
    brightness: Brightness.dark,
    primaryColor: Colors.brown[400],
    accentColor: Colors.cyan[600],

    // Define the default font family.
    fontFamily: 'Georgia',

    // Define the default TextTheme. Use this to specify the default
    // text styling for headlines, titles, bodies of text, and more.
    textTheme: TextTheme(
      headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
      headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
      bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
    ),
    colorScheme: ColorScheme(
      brightness: Brightness.dark,
      // Primary - The color displayed most frequently across your app’s screens and components.
      primary: Colors.brown[400],
      primaryVariant: Colors.brown[600],
      onPrimary: Colors.black,
      // Secondary - An accent color that, when used sparingly, calls attention to parts of your app.
      secondary: Colors.orange[400],
      secondaryVariant: Colors.orange[600],
      onSecondary: Colors.black,
      error: Colors.red[500],
      onError: Colors.black,
      background: Colors.grey[800],
      onBackground: Colors.black,
      // Surface - The background color for widgets like [Card].
      surface: Colors.grey[700],
      onSurface: Colors.grey[200],
    ),
  );
}
